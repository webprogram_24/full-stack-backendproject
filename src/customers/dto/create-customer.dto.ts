import { IsInt, IsNotEmpty, MinLength } from 'class-validator';
export class CreateCustomerDto {
  @IsNotEmpty()
  @MinLength(8)
  name: string;

  @IsNotEmpty()
  @IsInt()
  age: number;

  @IsNotEmpty()
  @MinLength(10)
  tel: string;

  @IsNotEmpty()
  @MinLength(1)
  gender: string;
}
